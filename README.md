# Mirror Monitor

Monitor the status of known mirrors of f-droid.org.



## Active Mirrors

* https://f-droid.org/repo
* rsync://mirror.f-droid.org/
* https://fdroid.tetaneutral.net/fdroid/repo
* https://mirror.cyberbits.eu/fdroid/repo
* https://bubu1.eu/fdroid/repo
* https://fdroid.swedneck.xyz/fdroid/repo
* https://ftp.fau.de/fdroid/repo
* http://fauftpffbmvh3p4h.onion/fdroid/repo
* https://mirror.jarsilio.com/fdroid/repo
* https://ftp.osuosl.org/pub/fdroid/repo
* https://mirror.scd31.com/fdroid/repo
* https://fdroid.fi-do.io/fdroid/repo
* https://plug-mirror.rcac.purdue.edu/fdroid/repo
* https://mirrors.tuna.tsinghua.edu.cn/fdroid/repo
* https://mirrors.nju.edu.cn/fdroid/repo